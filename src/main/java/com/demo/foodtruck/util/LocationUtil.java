/**
 * 
 */
package com.demo.foodtruck.util;

/**
 * @author Sudeep
 *
 */
public class LocationUtil { 

	/**
	 * Gives distance between two latitude and longitude coordinates in Meters.
	 * 
	 * @param fromLat
	 * @param fromLon
	 * @param toLat
	 * @param toLon
	 * @return distance between two coordinates.
	 */
	public static double distance(double fromLat, double fromLon, double toLat, double toLon) {
		
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
        
    }

}
