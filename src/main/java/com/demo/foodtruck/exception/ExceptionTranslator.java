/**
 * 
 */
package com.demo.foodtruck.exception;

import java.util.List;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.extern.slf4j.Slf4j;
/**
 * @author Sudeep
 *
 */
/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 */
@Slf4j
@ControllerAdvice
public class ExceptionTranslator {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorDto processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return processFieldErrors(fieldErrors);
	}

	@ExceptionHandler(BindException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorDto processValidationError(BindException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return processFieldErrors(fieldErrors);
	}

	@ExceptionHandler(CustomParameterizedException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ParameterizedErrorDto processParameterizedValidationError(CustomParameterizedException ex) {
		return ex.getErrorDto();
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDto processAccessDeniedExcpetion(AccessDeniedException e) {
		return new ErrorDto(ErrorConstants.ERR_ACCESS_DENIED, e.getMessage());
	}

	private ErrorDto processFieldErrors(List<FieldError> fieldErrors) {
		ErrorDto dto = new ErrorDto(ErrorConstants.ERR_VALIDATION);

		for (FieldError fieldError : fieldErrors) {
			dto.add(fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
		}

		return dto;
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ErrorDto processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
		return new ErrorDto(ErrorConstants.ERR_METHOD_NOT_SUPPORTED, exception.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDto> processRuntimeException(Exception ex) throws Exception {
		BodyBuilder builder;
		ErrorDto ErrorDto;
		ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
		if (responseStatus != null) {
			builder = ResponseEntity.status(responseStatus.value());
			ErrorDto = new ErrorDto("error." + responseStatus.value().value(), responseStatus.reason());
		} else {
			builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
			ErrorDto = new ErrorDto(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, "Internal server error");
		}
		log.error("Internal server error", ex);
		return builder.body(ErrorDto);
	}

	@ExceptionHandler(CustomException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorDto processCustomEpException(CustomException e) {
		log.info(e.getMessage());
		return new ErrorDto(e.getMessage(),e.getErrorCode());
	}

	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorDto processCustomValidationException(ValidationException e) {
		log.info("Validation failed");;
		return new ErrorDto("Validation failed", null, e.getMessages());
	}

	@ExceptionHandler(BadCredentialsException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public CustomLoginError processBadCredentials(BadCredentialsException bc) {
		return new CustomLoginError(HttpStatus.UNAUTHORIZED, "Invalid Credentials"); 
	}

}
