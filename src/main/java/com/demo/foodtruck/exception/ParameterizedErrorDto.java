/**
 * 
 */
package com.demo.foodtruck.exception;

/**
 * @author Sudeep
 *
 */
public class ParameterizedErrorDto {

	private final String message;
	private final String[] params;

	public ParameterizedErrorDto(String message, String... params) {
		this.message = message;
		this.params = params;
	}

	public String getMessage() {
		return message;
	}

	public String[] getParams() {
		return params;
	}


}
