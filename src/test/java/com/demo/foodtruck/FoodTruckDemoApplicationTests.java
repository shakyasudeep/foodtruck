package com.demo.foodtruck;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.foodtruck.controller.FoodTruckController;

@SpringBootTest
class FoodTruckDemoApplicationTests {
	
	@Autowired
	FoodTruckController foodTruckController;

	@Test
	void contextLoads() {
		
		assertThat(foodTruckController).isNotNull();
	}

}
