package com.demo.foodtruck.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.service.inf.LocationService;

import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

/**
 *
 * @author Sudeep
 */
@WebMvcTest(FoodTruckController.class)
public class FoodTruckControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LocationService locationService;

	Double latitude;
	Double longitude;
	EnhancedRandom enhancedRandomObject = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().collectionSizeRange(1, 2).build();

	List<FoodTruckDto> truckList;
	String objectid;

	@BeforeEach
	public void setUp() {

		latitude = new Double("37.7875398934675");

		longitude = new Double("-122.397726709152");

		truckList = enhancedRandomObject.objects(FoodTruckDto.class, 2, "").collect(Collectors.toList());

		objectid = truckList.get(0).getObjectid();
	}

	@Test
	public void getLocationFromService() throws Exception{

		when(locationService.listFoodTruckLocations(latitude, longitude)).thenReturn(truckList);		 

		this.mockMvc.perform(get("/get_location/"+latitude+"/"+longitude).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", hasSize(truckList.size())))
		.andExpect(jsonPath("$[0].objectid", is(objectid)));
	} 

}
