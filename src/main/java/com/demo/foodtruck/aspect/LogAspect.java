/**
 * 
 */
package com.demo.foodtruck.aspect;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Aspect
@Component
public class LogAspect {

	@Before("execution(* com.demo.foodtruck.controller.*.*(..))")
	public void allControllerCalled(JoinPoint joinPoint) {   	

		log.info("arguments " + Arrays.toString(joinPoint.getArgs()));
		log.info("Method " + joinPoint.getSignature().getName() + " called in controller");
	}

	@After("execution(* com.demo.foodtruck.controller.*.*(..))")
	public void allControllerExited(JoinPoint joinPoint) {
		log.info("Exited from method "+joinPoint.getSignature().getName());
	}	

}
