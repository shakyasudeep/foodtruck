/**
 * 
 */
package com.demo.foodtruck.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.service.inf.LocationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author Sudeep
 *
 */
@Controller
@Api(tags = "Location Detail APIs")
public class FoodTruckController {

	@Autowired
	LocationService locationService;

	@GetMapping("/get_location/{latitude}/{longitude}")
	@ApiOperation(value = "API to get near by location detail.", notes = "This API lists the location detail of Food Trucks nearest to the give coordinates(latitude and longitude) sorted in order of nearest point first.")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "latitude", required = true, dataTypeClass = Double.class, paramType = "path")	                    		,
		@ApiImplicitParam(name = "longitude", required = true, dataTypeClass = Double.class, paramType = "path")
	})
	public ResponseEntity<List<FoodTruckDto>> listNearByFoodTruckLocations(@PathVariable("latitude") Double latitude, @PathVariable("longitude") Double longitude) throws Exception{

		return new ResponseEntity<>(locationService.listFoodTruckLocations(latitude,longitude),HttpStatus.OK);
	}

}
