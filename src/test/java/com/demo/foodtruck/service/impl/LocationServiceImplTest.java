package com.demo.foodtruck.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.service.inf.LocationService;

import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import kong.unirest.MockClient;

/**
 *
 * @author Sudeep
 */
@SpringBootTest
public class LocationServiceImplTest {

	@Autowired
	private LocationService locationService;

	MockClient mock;

	EnhancedRandom enhancedRandomObject = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().collectionSizeRange(1, 2).build();

	List<FoodTruckDto> foodTruckResult;
	
	String locationUrl = "https://data.sfgov.org/resource/rqzj-sfat.json";	

	@BeforeEach
	public void setUp() {

		mock = MockClient.register();

		foodTruckResult = enhancedRandomObject.objects(FoodTruckDto.class, 2, "").collect(Collectors.toList());	

	}

	/**
	 * Test of listFoodTruckLocations method, of class LocationServiceImpl.
	 */
	@Test
	public void testListFoodTruckLocations() throws Exception {					 

		mock.expect(kong.unirest.HttpMethod.GET,locationUrl).queryString("facilitytype", "Truck")
		.thenReturn(foodTruckResult).withStatus(kong.unirest.
				HttpStatus.OK);		   

		List<FoodTruckDto> returnList = locationService.listFoodTruckLocations(233.3, -131.02);

		assertEquals(foodTruckResult.size(),returnList.size());	  

		mock.verifyAll();	


	}

}
