/**
 * 
 */
package com.demo.foodtruck.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.foodtruck.comparator.SortingLocations;
import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.exception.CustomException;
import com.demo.foodtruck.service.inf.LocationService;

import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.Unirest;
import kong.unirest.UnirestParsingException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */

@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

	private String locationUrl = "https://data.sfgov.org/resource/rqzj-sfat.json";

	@Override
	public List<FoodTruckDto> listFoodTruckLocations(Double givenLatitude, Double givenLongitude) throws Exception{

		log.info("Given Latitude => "+givenLatitude+" Given Longitude => "+givenLongitude);

		List<FoodTruckDto> foodTrucks = new ArrayList<>();		

		HttpResponse<List<FoodTruckDto>> foodTrucksResponse =
				Unirest.get(locationUrl) .queryString("facilitytype", "Truck") .asObject(new
						GenericType<List<FoodTruckDto>>(){});

		if (foodTrucksResponse.getStatus() == HttpStatus.OK) {

			foodTrucks = foodTrucksResponse.getBody();

			foodTrucks.sort(new SortingLocations(givenLatitude, givenLongitude));

		} else { 
			
			UnirestParsingException ex = foodTrucksResponse.getParsingError().get();
			log.error("Error Message => "+ex.getMessage());
			log.error("Error Cause => "+ex.getCause());
			log.error("Error Original Body => "+ex.getOriginalBody()); 

			throw new CustomException("exc001"); 
		}


		return foodTrucks;

	}

}
