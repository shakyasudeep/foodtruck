package com.demo.foodtruck.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PropertiesUtil { 

	public static Properties readProperties(String fileName) throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream input = classLoader.getResourceAsStream(fileName);

		Properties prop = new Properties();
		prop.load(input);

		return prop;
	}

	public static String getMessage(String key, String file) {
		try {
			Properties prop = readProperties(file);
			return prop.getProperty(key);
		} catch (Exception e) {
			log.error("Error occurred while reading file "+file, e);
		}

		return null;
	}   

	public static String getMessageFromErrorCode(String errorCode){
		return PropertiesUtil.getMessage(errorCode, "errorCodes.properties");
	}

}
