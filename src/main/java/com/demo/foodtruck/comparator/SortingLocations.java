/**
 * 
 */
package com.demo.foodtruck.comparator;

import java.util.Comparator;

import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.util.LocationUtil;

/**
 * @author Sudeep
 *
 */
public class SortingLocations implements Comparator<FoodTruckDto> {
	
	Double givenLatitude;
	Double givenLongitude;
	
	/**
	 * @param latitude
	 * @param longitude
	 */
	public SortingLocations(Double latitude, Double longitude) {
		givenLatitude = latitude;
		givenLongitude = longitude;
	}

	/**
	 *The distance between two coordinates is calculated using Great-circle distance formula.
	 *https://en.wikipedia.org/wiki/Great-circle_distance
	 */
	@Override
	public int compare(FoodTruckDto o1, FoodTruckDto o2) {

		double lat1 = o1.latitude;
        double lon1 = o1.longitude;
        double lat2 = o2.latitude;
        double lon2 =o2.longitude;

        double distanceToPlace1 = LocationUtil.distance(givenLatitude, givenLongitude, lat1, lon1);
        double distanceToPlace2 = LocationUtil.distance(givenLatitude, givenLongitude, lat2, lon2);
        
        return (int) (distanceToPlace1 - distanceToPlace2);
		
	}

}
