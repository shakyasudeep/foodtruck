# Food Trucks

This project is a demo project for demonstrating API development based on Spring Boot along with relevant technologies. So this is basically a **SpringBoot Backend Appplication** along with **Swagger UI** documentation.
It uses [Socrata Open Data API (SODA)](https://dev.socrata.com/foundry/data.sfgov.org/rqzj-sfat) open API to get data of food trucks available at certain location.
The project is hosted on Digital Ocean for time being.
[Click Here](http:142.93.93.240:8080/foodTruck/swagger-ui/) to view the project.


# Technologies
This project is created with :
* [Java](https://www.oracle.com/java/technologies/java8.html) version: 1.8
* [SpringBoot](https://spring.io/projects/spring-boot) version: 2.3.4.RELEASE
* [Unirest](https://kong.github.io/unirest-java/#requests) library version: 3.10.00
* [SpringFox Boot Starter](https://github.com/springfox/springfox) library version: 3.0.0
* [Random Beans](https://github.com/j-easy/easy-random) library version: 3.9.0
* [Lombok](https://projectlombok.org/) library version: 1.18.12
* [JUnit](https://junit.org/junit5/docs/current/user-guide/) version: 5
* Maven
* And other relevant technologies.

We have chosen Java/Spring Boot because Spring Boot framework is based on Java’s Spring Framework which was introduced before around 15 years. So it is Mature. It is lightweight as compared to other Java stacks. So, maturity + lightweight makes a great combination, and that is one strong reason to choose Spring Boot over any other technologies. It's open source. And it's convention over configuration approach solves the complexity while setting up the project. As well as it is enterprise ready framework. The community support is large.

## Feature

This project contains a single API to list the location details of the **FoodTrucks**
based on given location coordinates(latitude and longitude) and the result is sorted based on the nearest coordinates first with reference to the given coordinates. The documentation is provided using [Swagger.IO](https://swagger.io/) which can be accessed via *****baseUrl/swagger-ui/*****(eg: http://localhost:8080/swagger-ui/) which describes the feature of the API and the data type returned by the API.
![Swagger UI](https://ibb.co/nCWg4XJ)

This example would be useful for listing nearest location coordinates reference to the given coordinates. This solution can be very useful for finding near by places like restaurants, colleges, schools, landmarks, hospitals, cinema halls, etc by simple modifications based on listed categories like above examples. So it's kind of nearest location first and also the listing the locations to certain radius/distance with some customization.
The distance between two coordinates is calculated using [Great-circle distance](https://en.wikipedia.org/wiki/Great-circle_distance) formula.
This project uses Factory Design Pattern and other defaults with the framework.
The exceptions are handled across the whole application using @ControllerAdvice along with custom exception classes.
The API endpoint is not secured yet but the project includes security configuration which can be tweaked to add authorization to the endpoints easily. For now all the endpoints are allowed. There are no users created, only includes the default user and password generated at runtime by default.

## Running the application
It needs Maven previously installed to the run the application.
Simple run the application with built in Tomcat server with the command :

    mvn spring-boot:run
It will run on default Tomcat port 8080 and the Swagger documentation can be accessed from [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/) to test the API endpoint. Currently the API doesn't need any authorization.

## Deploying the application
You can package the application to a WAR file and deploy to server (eg: Tomcat, Glassfish,etc..) by running following command(if you don't want to skip test, just run  `mvn package`):```

    mvn package -Dmaven.test.skip=true

Or if you want to run as a standalone JAR, change the packaging in `pom.xml` to :

`<``project` `...>`

`...`

`<``packaging``>jar</``packaging``>`

`...`

`</``project``>`

and run the same command as above to package to JAR file which is created in the project’s `target` directory.. 
To run the packaged JAR run :

    java –jar YourProjectName-version.jar

## Developer
 - Sudeep Shakya([shakyasudeep@live.com](mailto:shakyasudeep@live.com)), [LinkedIn](https://www.linkedin.com/in/sudeep-shakya-705a3251/)

